package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/Pallinder/go-randomdata"
	"golang.org/x/net/proxy"
)

func loadConfig(file string) *Config {
	configFile, err := os.Open(file)
	if err != nil {
		log.Fatal("opening config file", err)
	}

	var config *Config

	jsonParser := json.NewDecoder(configFile)
	if err = jsonParser.Decode(&config); err != nil {
		log.Fatal("parsing config file", err)
	}

	return config
}

func getUH(html []byte) (string, error) {
	re := regexp.MustCompile(`<input type="hidden" name="uh" value="\w+"`)

	input := re.Find(html)

	// do some ugly shit to get the token
	// ima update this bot its a rough draft
	splits := strings.Split(string(input), "<input type=\"hidden\" name=\"uh\" value=\"")

	if len(splits) < 2 {
		return "", errors.New("Could not get uh")
	}

	return strings.Split(splits[1], "\"")[0], nil
}

func getIden(html []byte) (string, error) {
	re := regexp.MustCompile(`<input name="iden" value="\w+"`)

	input := re.Find(html)

	// do some ugly shit to get the token
	// ima update this bot its a rough draft
	splits := strings.Split(string(input), "<input name=\"iden\" value=\"")

	if len(splits) < 2 {
		return "", errors.New("Could not get iden")
	}

	return strings.Split(splits[1], "\"")[0], nil
}

func getCaptchaURL(html []byte) (string, error) {
	re := regexp.MustCompile(`<img class="capimage" alt="visual CAPTCHA" src="/\w+/\w+.png"`)

	input := re.Find(html)

	// do some ugly shit to get the token
	// ima update this bot its a rough draft
	splits := strings.Split(string(input), "<img class=\"capimage\" alt=\"visual CAPTCHA\" src=\"")

	if len(splits) < 2 {
		return "", errors.New("Could not get captcha")
	}

	return strings.Split(splits[1], "\"")[0], nil
}

func generateUsername(myClient *http.Client) string {
	name := randomdata.SillyName() + randomString(100, 5000)

	v := url.Values{}

	v.Add("user", name)

	resp, err := myClient.PostForm(UsernameCheckURL, v)
	if err != nil {
		log.Fatal("error posring", err)
	}

	r, _ := ioutil.ReadAll(resp.Body)

	if string(r) == "{}" {
		return name
	}

	fmt.Println("Username exists", name)

	time.Sleep(3 * time.Second)

	return generateUsername(myClient)
}

func makeSocks5Client(file string) *http.Client {
	// Set the cookie jar
	cookieJar, err := cookiejar.New(nil)
	if err != nil {
		fmt.Println("Error setting up cooking jar", err)
	}

	servers := getProxyList(file)

	dialSocksProxy, err := proxy.SOCKS5("tcp", servers[random(0, len(servers)-1)], nil, proxy.Direct)
	if err != nil {
		fmt.Println("Error connecting to proxy:", err)
	}
	tr := &http.Transport{Dial: dialSocksProxy.Dial}

	// Create client
	client := &http.Client{
		Jar:       cookieJar,
		Transport: tr,
		Timeout:   time.Duration(30 * time.Second),
	}

	if checkClient(client) {
		return client
	}

	fmt.Println("proxy not working")

	return makeSocks5Client(file)
}

func checkClient(client *http.Client) bool {
	resp, err := client.Get("http://google.com")
	if err != nil {
		fmt.Println("proxy error:", err)
		return false
	} else if resp.StatusCode != 200 {
		return false
	}
	return true
}

func getProxyList(fileName string) []string {
	data, err := ioutil.ReadFile(fileName)
	if err != nil {
		fmt.Println("Error opening file:", err)
	}
	return strings.Split(string(data), "\n")
}

func randomString(min, max int) string {
	rand.Seed(time.Now().Unix())
	return strconv.Itoa(rand.Intn(max-min) + min)

}

func random(min, max int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(max-min) + min
}
