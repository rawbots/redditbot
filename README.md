## Redditbot

# usage

If you know how to use git follow these steps.

Clone the and cd into the repo

``` bash
git clone https://gitlab.com/rodzzlessa24/redditbot.git
cd redditbot
```

Rename the `config.example.json` to `config.json`

```bash
mv config.example.json config.json
```

The config file holds:
 - proxy file path
 - reddit csv file path
 - death by captcha username
 - death by captcha password

Update the `reddit.csv` to the threads you wanna post `link, title, subreddit`. Make sure for the subreddit you just put the name and not the full url

Now we can run the bot.

Run the executable and pass in the config.json path

```bash
./redditbot config.json
```

Bot should now be running
