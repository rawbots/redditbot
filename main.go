package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/antonyho/go-deathbycaptcha"
)

type Config struct {
	ProxyFile   string `json:"proxy_file"`
	RedditCSV   string `json:"reddit_csv"`
	DBCUsername string `json:"dbc_username"`
	DBCPassword string `json:"dbc_password"`
}

const (
	RedditURL        = "https://www.reddit.com"
	RegisterURL      = "https://www.reddit.com/api/register/" // + username
	UsernameCheckURL = "https://www.reddit.com/api/check_username.json"
	NewThreadURL     = "https://www.reddit.com/submit"
)

func main() {

	// Do some validation things we needed were passed in
	if len(os.Args) < 2 {
		log.Fatal("Please provie a config.json file")
	}

	config := loadConfig(os.Args[1])

	// Load the reddit CSV
	csvfile, err := os.Open(config.RedditCSV)

	if err != nil {
		log.Fatal("Error opening reddit csv file", err)
	}
	defer csvfile.Close()

	// read the csv file
	r := csv.NewReader(csvfile)

	// Loop over all the csv inputs
	for {
		thread, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal("error reading csv file:", err)
		}

		// Create a new SOCKS5 proxy client
		myClient := makeSocks5Client(config.ProxyFile)

		/* Register to reddit */
		// Generate username
		username := generateUsername(myClient)

		rv := url.Values{}
		rv.Add("op", "reg")
		rv.Add("dest", "https://www.reddit.com/")
		rv.Add("user", username)
		rv.Add("passwd", "yoloAdmin13!")
		rv.Add("passwd2", "yoloAdmin13!")
		rv.Add("email", "")
		rv.Add("api_type", "json")

		resp, err := myClient.PostForm(RegisterURL+username, rv)
		if err != nil {
			fmt.Println("Error registering user", err)
			continue
		}

		rhtml, _ := ioutil.ReadAll(resp.Body)

		if strings.Contains(string(rhtml), "RATELIMIT") {
			fmt.Println("Bot got ratelimited so we gotta sleep for 5 minutes")
			time.Sleep(5 * time.Minute)
			continue
		}

		fmt.Println("user " + username + " registered")

		time.Sleep(3 * time.Second)

		/* POST A NEW THREAD */
		fmt.Println("posting new thread...")
		// Get the post thread page
		resp, err = myClient.Get(NewThreadURL)
		if err != nil {
			fmt.Println("Error getting new thread page", err)
			continue
		}

		time.Sleep(3 * time.Second)

		html, _ := ioutil.ReadAll(resp.Body)

		// ioutil.WriteFile("test.html", html, 0755)

		if strings.Contains(string(html), "log in") || strings.Contains(string(html), "we're sorry") {
			fmt.Println("bot blocked sleeping for a minute and moving on to next link")
			time.Sleep(60 * time.Second)
			continue
		}

		fmt.Println("filling out new thread form...")

		// Get the image link
		captchaURL, err := getCaptchaURL(html)
		if err != nil {
			fmt.Println(err)
			continue
		}

		uh, err := getUH(html)
		if err != nil {
			fmt.Println(err)
			continue
		}

		iden, err := getIden(html)
		if err != nil {
			fmt.Println(err)
			continue
		}

		resp, err = myClient.Get(RedditURL + captchaURL)
		if err != nil {
			fmt.Println(err)
			continue
		}

		img, _ := ioutil.ReadAll(resp.Body)

		ioutil.WriteFile("captcha.png", img, 0755)

		fmt.Println("sending captcha to death by captcha...")

		dbc := deathbycaptcha.New()
		solvedCaptcha, err := dbc.Solve(config.DBCUsername, config.DBCPassword, "captcha.png")

		tv := url.Values{}
		tv.Add("uh", uh)
		tv.Add("title", thread[1])
		tv.Add("kind", "link")
		tv.Add("url", thread[0])
		tv.Add("sr", thread[2])
		tv.Add("sendreplies", "true")
		tv.Add("iden", iden)
		tv.Add("captcha", solvedCaptcha)
		tv.Add("resubmit", "")
		tv.Add("id", "#newlink")
		tv.Add("renderstyle", "html")

		fmt.Println("submiting new thread...")

		_, err = myClient.PostForm("https://www.reddit.com/api/submit", tv)
		if err != nil {
			fmt.Println("error posting new thread:", err)
			continue
		}

		fmt.Println("new thread submission success!")
		time.Sleep(2 * time.Second)
	}

}
